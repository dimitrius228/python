# -*- coding: utf-8 -*-

# Создать прототип игры Алхимия: при соединении двух элементов получается новый.
# Реализовать следующие элементы: Вода, Воздух, Огонь, Земля, Шторм, Пар, Грязь, Молния, Пыль, Лава.
# Каждый элемент организовать как отдельный класс.
# Таблица преобразований:
#   Вода + Воздух = Шторм
#   Вода + Огонь = Пар
#   Вода + Земля = Грязь
#   Воздух + Огонь = Молния
#   Воздух + Земля = Пыль
#   Огонь + Земля = Лава

# Сложение элементов реализовывать через __add__
# Если результат не определен - то возвращать None
# Вывод элемента на консоль реализовывать через __str__
#
# Примеры преобразований:
#   print(Water(), '+', Air(), '=', Water() + Air())
#   print(Fire(), '+', Air(), '=', Fire() + Air())

class Water:
    class_name = "Water"

    def __str__(self):
        return "Water"

    def __eq__(self, other):
        return self.class_name == other.class_name

    def __add__(self, other):
        if other == Air():
            return Storm()
        elif other == Water():
            return Water()
        elif other == Fire():
            return Air()
        elif other == Ground():
            return Mud()
        else:
            return None


class Air:
    class_name = "Air"

    def __str__(self):
        return "Air"

    def __eq__(self, other):
        return self.class_name == other.class_name

    def __add__(self, other):
        if other == Air():
            return Air()
        elif other == Water():
            return Storm()
        elif other == Fire():
            return Lightning()
        elif other == Ground():
            return Dust()
        else:
            return None


class Fire:
    class_name = "Fire"

    def __str__(self):
        return "Fire"

    def __eq__(self, other):
        return self.class_name == other.class_name

    def __add__(self, other):
        if other == Air():
            return Lightning()
        elif other == Water():
            return Vapor()
        elif other == Fire():
            return Fire()
        elif other == Ground():
            return Lava()
        else:
            return None


class Ground:
    class_name = "Ground"

    def __str__(self):
        return "Ground"

    def __eq__(self, other):
        return self.class_name == other.class_name

    def __add__(self, other):
        if other == Air():
            return Dust()
        elif other == Water():
            return Mud()
        elif other == Fire():
            return Lava()
        elif other == Ground():
            return Ground()
        else:
            return None


class Storm:
    class_name = "Storm"

    def __str__(self):
        return "Storm"

    def __eq__(self, other):
        return self.class_name == other.class_name


class Vapor:
    class_name = "Vapor"

    def __str__(self):
        return "Vapor"

    def __eq__(self, other):
        return self.class_name == other.class_name


class Mud:
    class_name = "Mud"

    def __str__(self):
        return "Mud"

    def __eq__(self, other):
        return self.class_name == other.class_name


class Lightning:
    class_name = "Lightning"

    def __str__(self):
        return "Lightning"

    def __eq__(self, other):
        return self.class_name == other.class_name


class Dust:
    class_name = "Dust"

    def __str__(self):
        return "Dust"

    def __eq__(self, other):
        return self.class_name == other.class_name


class Lava:
    class_name = "Lava"

    def __str__(self):
        return "Lava"

    def __eq__(self, other):
        return self.class_name == other.class_name


print(Water(), '+', Air(), '=', Water() + Air())
print(Water(), '+', Fire(), '=', Water() + Fire())
print(Water(), '+', Ground(), '=', Water() + Lava())
print(Air(), '+', Fire(), '=', Fire() + Air())
print(Air(), '+', Ground(), '=', Ground() + Air())
print(Fire(), '+', Ground(), '=', Fire() + Ground())
