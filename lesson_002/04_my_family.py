#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pprint import pprint

# Создайте списки:

# моя семья (минимум 3 элемента, есть еще дедушки и бабушки, если что)
my_family = []
my_family = ['Father', 'Mother', "Brother", "Sister", "Grandmother", "Grandfather"]

# список списков приблизителного роста членов вашей семьи
my_family_height = [
    ['Father', 190], ['Mother', 180], ['Brother', 150], ['Sister', 130], ['Grandmother', 169], ['Grandfather', 185],
]
# Выведите на консоль рост отца в формате
#   Рост отца - ХХ см
print('Рост отца - ', my_family_height[0][1], 'см')

# Выведите на консоль общий рост вашей семьи как сумму ростов всех членов
#   Общий рост моей семьи - ХХ см
all_height = my_family_height[0][1] + my_family_height[1][1] + my_family_height[2][1] + my_family_height[3][1] + \
             my_family_height[4][1] + my_family_height[5][1]
print('Общий рост моей семьи - ', all_height, 'см')
