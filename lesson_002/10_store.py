#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Есть словарь кодов товаров

goods = {
    'Лампа': '12345',
    'Стол': '23456',
    'Диван': '34567',
    'Стул': '45678',
}

# Есть словарь списков количества товаров на складе.

store = {
    '12345': [
        {'quantity': 27, 'price': 42},
    ],
    '23456': [
        {'quantity': 22, 'price': 510},
        {'quantity': 32, 'price': 520},
    ],
    '34567': [
        {'quantity': 2, 'price': 1200},
        {'quantity': 1, 'price': 1150},
    ],
    '45678': [
        {'quantity': 50, 'price': 100},
        {'quantity': 12, 'price': 95},
        {'quantity': 43, 'price': 97},
    ],
}

# Рассчитать на какую сумму лежит каждого товара на складе
# например для ламп

lamps_cost = store[goods['Лампа']][0]['quantity'] * store[goods['Лампа']][0]['price']
# или проще (/сложнее ?)
lamp_code = goods['Лампа']
lamps_item = store[lamp_code][0]
lamps_quantity = lamps_item['quantity']
lamps_price = lamps_item['price']
lamps_cost = lamps_quantity * lamps_price
print('Лампа -', lamps_quantity, 'шт, стоимость', lamps_cost, 'руб')

# Вывести стоимость каждого товара на складе: один раз распечать сколько всего столов, стульев и т.д. на складе
# Формат строки <товар> - <кол-во> шт, стоимость <общая стоимость> руб

# WARNING для знающих циклы: БЕЗ циклов. Да, с переменными; да, неэффективно; да, копипаста.
# Это задание на ручное вычисление - что бы потом понять как работают циклы и насколько с ними проще жить.

# TODO здесь ваш код
my_lamps_cost = store[goods['Лампа']][0]['quantity'] * store[goods['Лампа']][0]['price']
print('Лампа -', store[goods['Лампа']][0]['quantity'], 'шт, стоимость', my_lamps_cost, 'руб')

my_table1_quantity = store[goods['Стол']][0]['quantity']
my_table2_quantity = store[goods['Стол']][1]['quantity']
my_table1_cost = my_table1_quantity * store[goods['Стол']][0]['price']
my_table2_cost = my_table2_quantity * store[goods['Стол']][1]['price']
my_table_quantity = my_table1_quantity + my_table2_quantity
my_table_cost = my_table1_cost + my_table2_cost
print('Стол -', my_table_quantity, 'шт, стоимость', my_table_cost, 'руб')

my_sofa1_quantity = store[goods['Диван']][0]['quantity']
my_sofa2_quantity = store[goods['Диван']][1]['quantity']
my_sofa1_cost = my_sofa1_quantity * store[goods['Диван']][0]['price']
my_sofa2_cost = my_sofa2_quantity * store[goods['Диван']][1]['price']
my_sofa_quantity = my_sofa1_quantity + my_sofa2_quantity
my_sofa_cost = my_sofa1_cost + my_sofa2_cost
print('Диван -', my_sofa_quantity, 'шт, стоимость', my_sofa_cost, 'руб')

my_chair1_quantity = store[goods['Стул']][0]['quantity']
my_chair2_quantity = store[goods['Стул']][1]['quantity']
my_chair3_quantity = store[goods['Стул']][2]['quantity']
my_chair1_cost = my_chair1_quantity * store[goods['Стул']][0]['price']
my_chair2_cost = my_chair2_quantity * store[goods['Стул']][1]['price']
my_chair3_cost = my_chair3_quantity * store[goods['Стул']][2]['price']
my_chair_quantity = my_chair1_quantity + my_chair2_quantity + my_chair3_quantity
my_chair_cost = my_chair1_cost + my_chair2_cost + my_chair3_cost
print('Стул -', my_chair_quantity, 'шт, стоимость', my_chair_cost, 'руб')

##########################################################################################
# ВНИМАНИЕ! После того как __ВСЯ__ домашняя работа сделана и запушена на сервер,         #
# нужно зайти в ЛМС (LMS - Learning Management System ) по адресу http://go.skillbox.ru  #
# и оформить попытку сдачи ДЗ! Без этого ДЗ не будет проверяться!                        #
# Как оформить попытку сдачи смотрите видео - https://youtu.be/qVpN0L-C3LU               #
##########################################################################################
