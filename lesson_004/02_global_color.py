# -*- coding: utf-8 -*-
import simple_draw as sd


# Добавить цвет в функции рисования геом. фигур. из упр lesson_004/01_shapes.py
# (код функций скопировать сюда и изменить)
# Запросить у пользователя цвет фигуры посредством выбора из существующих:
#   вывести список всех цветов с номерами и ждать ввода номера желаемого цвета.
# Потом нарисовать все фигуры этим цветом

# Пригодятся функции
# sd.get_point()
# sd.line()
# sd.get_vector()
# и константы COLOR_RED, COLOR_ORANGE, COLOR_YELLOW, COLOR_GREEN, COLOR_CYAN, COLOR_BLUE, COLOR_PURPLE
# Результат решения см lesson_004/results/exercise_02_global_color.jpg


def all_point(figure, angle_figure, start_point, angle, length_sides, color):
    start_point_vector = sd.get_point(*start_point)
    for _ in range(figure):
        end_point = sd.vector(start=start_point_vector, angle=angle, length=length_sides, color=color)
        angle += angle_figure
        start_point_vector = end_point
        sd.circle(end_point, radius=2, color=color)


def triangle(start_point, angle, length_sides, color):
    figure, angle_figure = 3, 120
    all_point(figure=figure, angle_figure=angle_figure, color=color, start_point=start_point, angle=angle,
              length_sides=length_sides)


def square(start_point, angle, length_sides, color):
    figure, angle_figure = 4, 90
    all_point(figure=figure, angle_figure=angle_figure, color=color, start_point=start_point, angle=angle,
              length_sides=length_sides)


def pentagon(start_point, angle, length_sides, color):
    figure, angle_figure = 5, 72
    all_point(figure=figure, angle_figure=angle_figure, color=color, start_point=start_point, angle=angle,
              length_sides=length_sides)


def hexagon(start_point, angle, length_sides, color):
    figure, angle_figure = 6, 60
    all_point(figure=figure, angle_figure=angle_figure, color=color, start_point=start_point, angle=angle,
              length_sides=length_sides)


color_print = {'0': "COLOR_PURPLE", '1': "COLOR_RED", '2': "COLOR_ORANGE", '3': "COLOR_YELLOW", '4': "COLOR_GREEN",
               '5': "COLOR_CYAN", '6': "COLOR_BLUE"}
colors = {'0': sd.COLOR_PURPLE, '1': sd.COLOR_RED, '2': sd.COLOR_ORANGE, '3': sd.COLOR_YELLOW, '4': sd.COLOR_GREEN,
          '5': sd.COLOR_CYAN, '6': sd.COLOR_BLUE}

print("Возможные цвета:")
for i in color_print:
    print(i, ":", color_print[i])
while True:
    input_color = input('Введите желаемый цвет >')
    if input_color in color_print:
        break
    else:
        print("Вы ввели некоректный номер ")

start_point_triangle = (100, 200)
start_point_square = (400, 200)
start_point_pentagon = (100, 400)
start_point_hexagon = (400, 400)
triangle(start_point=start_point_triangle, color=colors[input_color], angle=20, length_sides=100)
square(start_point=start_point_square, color=colors[input_color], angle=20, length_sides=100)
pentagon(start_point=start_point_pentagon, color=colors[input_color], angle=20, length_sides=100)
hexagon(start_point=start_point_hexagon, color=colors[input_color], angle=20, length_sides=100)
print("Выполнено")
sd.pause()
