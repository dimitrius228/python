# -*- coding: utf-8 -*-
from random import randint

import simple_draw as sd
from pprint import pprint

# На основе кода из практической части реализовать снегопад:
# - создать списки данных для отрисовки N снежинок
# - нарисовать падение этих N снежинок
# - создать список рандомных длинн лучей снежинок (от 10 до 100) и пусть все снежинки будут разные

N = 20

# Пригодятся функции
# sd.get_point()
# sd.snowflake()
# sd.sleep()
# sd.random_number()
# sd.user_want_exit()
my_dict_snowflake = {}
for i in range(N):
    my_dict_snowflake.setdefault(i, {"length": sd.random_number(10, 100), "factor_a": sd.random_number(10, 100) / 100,
                                     "factor_b": sd.random_number(10, 100) / 100, "factor_c": sd.random_number(10, 170),
                                     "point_x": sd.random_number(100, 500), "point_y": 500})

key = 0
x, y = my_dict_snowflake[key]["point_x"], my_dict_snowflake[key]["point_y"]

while True:
    point = sd.get_point(x, y)
    sd.start_drawing()
    sd.snowflake(center=point, length=my_dict_snowflake[key]["length"], factor_a=my_dict_snowflake[key]["factor_a"],
                 factor_b=my_dict_snowflake[key]["factor_b"], factor_c=my_dict_snowflake[key]["factor_c"])
    sd.snowflake(center=point, length=my_dict_snowflake[key]["length"], factor_a=my_dict_snowflake[key]["factor_a"],
                 factor_b=my_dict_snowflake[key]["factor_b"], factor_c=my_dict_snowflake[key]["factor_c"],
                 color=sd.background_color)
    y -= 40
    x = x + 10 * (sd.random_number(-100, 100) / 100)
    point = sd.get_point(x, y)
    sd.snowflake(center=point, length=my_dict_snowflake[key]["length"], factor_a=my_dict_snowflake[key]["factor_a"],
                 factor_b=my_dict_snowflake[key]["factor_b"], factor_c=my_dict_snowflake[key]["factor_c"])

    if y < 50:
        key = key + 1
        x, y = my_dict_snowflake[key]["point_x"], my_dict_snowflake[key]["point_y"]
        if key == 19:
            key = key - 19
    sd.finish_drawing()
    sd.sleep(0.1)
    if sd.user_want_exit():
        break

sd.pause()

# подсказка! для ускорения отрисовки можно
#  - убрать clear_screen()
#  - в начале рисования всех снежинок вызвать sd.start_drawing()
#  - на старом месте снежинки отрисовать её же, но цветом sd.background_color
#  - сдвинуть снежинку
#  - отрисовать её цветом sd.COLOR_WHITE на новом месте
#  - после отрисовки всех снежинок, перед sleep(), вызвать sd.finish_drawing()


# 4) Усложненное задание (делать по желанию)
# - сделать рандомные отклонения вправо/влево при каждом шаге
# - сделать сугоб внизу экрана - если снежинка долетает до низа, оставлять её там,
#   и добавлять новую снежинку
# Результат решения см https://youtu.be/XBx0JtxHiLg
