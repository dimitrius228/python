# -*- coding: utf-8 -*-

import simple_draw as sd


# Часть 1.
# Написать функции рисования равносторонних геометрических фигур:
# - треугольника
# - квадрата
# - пятиугольника
# - шестиугольника
# Все функции должны принимать 3 параметра:
# - точка начала рисования
# - угол наклона
# - длина стороны
#
# Использование копи-пасты - обязательно! Даже тем кто уже знает про её пагубность. Для тренировки.
# Как работает копипаста:
#   - одну функцию написали,
#   - копипастим её, меняем название, чуть подправляем код,
#   - копипастим её, меняем название, чуть подправляем код,
#   - и так далее.
# В итоге должен получиться ПОЧТИ одинаковый код в каждой функции

# Пригодятся функции
# sd.get_point()
# sd.vector()
# sd.line()
# Результат решения см lesson_004/results/exercise_01_shapes.jpg
# Часть 1-бис.
# Попробуйте прикинуть обьем работы, если нужно будет внести изменения в этот код.
# Скажем, связывать точки не линиями, а дугами. Или двойными линиями. Или рисовать круги в угловых точках. Или...
# А если таких функций не 4, а 44?

# def triangle(start_point, angle, length_sides):
#     start_point_vector = sd.get_point(*start_point)
#     for _ in range(3):
#         end_point = sd.vector(start=start_point_vector, angle=angle, length=length_sides)
#         angle += 120
#         start_point_vector = end_point
#         sd.circle(end_point, radius=2)
#
#
# def square(start_point, angle, length_sides):
#     start_point_vector = sd.get_point(*start_point)
#     for _ in range(4):
#         end_point = sd.vector(start=start_point_vector, angle=angle, length=length_sides)
#         angle += 90
#         start_point_vector = end_point
#         sd.circle(end_point, radius=2)
#
#
# def pentagon(start_point, angle, length_sides):
#     start_point_vector = sd.get_point(*start_point)
#     for _ in range(5):
#         end_point = sd.vector(start=start_point_vector, angle=angle, length=length_sides)
#         angle += 72
#         start_point_vector = end_point
#         sd.circle(end_point, radius=2)
#
#
# def hexagon(start_point, angle, length_sides):
#     start_point_vector = sd.get_point(*start_point)
#     for _ in range(6):
#         end_point = sd.vector(start=start_point_vector, angle=angle, length=length_sides)
#         angle += 60
#         start_point_vector = end_point
#         sd.circle(end_point, radius=2)
#
#
# start_point_triangle = (100, 200)
# start_point_square = (400, 200)
# start_point_pentagon = (100, 400)
# start_point_hexagon = (400, 400)
# triangle(start_point=start_point_triangle, angle=20, length_sides=100)
# square(start_point=start_point_square, angle=20, length_sides=100)
# pentagon(start_point=start_point_pentagon, angle=20, length_sides=100)
# hexagon(start_point=start_point_hexagon, angle=20, length_sides=100)


# Часть 2 (делается после зачета первой части)
#
# Надо сформировать функцию, параметризированную в местах где была "небольшая правка".
# Это называется "Выделить общую часть алгоритма в отдельную функцию"
# Потом надо изменить функции рисования конкретных фигур - вызывать общую функцию вместо "почти" одинакового кода.
#
# В итоге должно получиться:
#   - одна общая функция со множеством параметров,
#   - все функции отрисовки треугольника/квадрата/етс берут 3 параметра и внутри себя ВЫЗЫВАЮТ общую функцию.
#
# Не забудте в этой общей функции придумать, как устранить разрыв
#   в начальной/конечной точках рисуемой фигуры (если он есть)

# Часть 2-бис.
# А теперь - сколько надо работы что бы внести изменения в код? Выгода на лицо :)
# Поэтому среди программистов есть принцип D.R.Y. https://clck.ru/GEsA9
# Будьте ленивыми, не используйте копи-пасту!
def all_point(figure, angle_figure, start_point, angle, length_sides):
    start_point_vector = sd.get_point(*start_point)
    for _ in range(figure):
        end_point = sd.vector(start=start_point_vector, angle=angle, length=length_sides)
        angle += angle_figure
        start_point_vector = end_point
        sd.circle(end_point, radius=2)


def triangle(start_point, angle, length_sides):
    figure, angle_figure = 3, 120
    all_point(figure=figure, angle_figure=angle_figure, start_point=start_point, angle=angle, length_sides=length_sides)


def square(start_point, angle, length_sides):
    figure, angle_figure = 4, 90
    all_point(figure=figure, angle_figure=angle_figure, start_point=start_point, angle=angle, length_sides=length_sides)


def pentagon(start_point, angle, length_sides):
    figure, angle_figure = 5, 72
    all_point(figure=figure, angle_figure=angle_figure, start_point=start_point, angle=angle, length_sides=length_sides)


def hexagon(start_point, angle, length_sides):
    figure, angle_figure = 6, 60
    all_point(figure=figure, angle_figure=angle_figure, start_point=start_point, angle=angle, length_sides=length_sides)


start_point_triangle = (100, 200)
start_point_square = (400, 200)
start_point_pentagon = (100, 400)
start_point_hexagon = (400, 400)
triangle(start_point=start_point_triangle, angle=20, length_sides=100)
square(start_point=start_point_square, angle=20, length_sides=100)
pentagon(start_point=start_point_pentagon, angle=20, length_sides=100)
hexagon(start_point=start_point_hexagon, angle=20, length_sides=100)

sd.pause()
