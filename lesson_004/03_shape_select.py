# -*- coding: utf-8 -*-

import simple_draw as sd


# Запросить у пользователя желаемую фигуру посредством выбора из существующих
#   вывести список всех фигур с номерами и ждать ввода номера желаемой фигуры.
# и нарисовать эту фигуру в центре экрана

# Код функций из упр lesson_004/02_global_color.py скопировать сюда
# Результат решения см lesson_004/results/exercise_03_shape_select.jpg

def all_point(figure, angle_figure, start_point, angle, length_sides, color):
    start_point_vector = sd.get_point(*start_point)
    for _ in range(figure):
        end_point = sd.vector(start=start_point_vector, angle=angle, length=length_sides, color=color)
        angle += angle_figure
        start_point_vector = end_point
        sd.circle(end_point, radius=2, color=color)


def triangle(start_point, angle, length_sides, color):
    figure, angle_figure = 3, 120
    all_point(figure=figure, angle_figure=angle_figure, color=color, start_point=start_point, angle=angle,
              length_sides=length_sides)


def square(start_point, angle, length_sides, color):
    figure, angle_figure = 4, 90
    all_point(figure=figure, angle_figure=angle_figure, color=color, start_point=start_point, angle=angle,
              length_sides=length_sides)


def pentagon(start_point, angle, length_sides, color):
    figure, angle_figure = 5, 72
    all_point(figure=figure, angle_figure=angle_figure, color=color, start_point=start_point, angle=angle,
              length_sides=length_sides)


def hexagon(start_point, angle, length_sides, color):
    figure, angle_figure = 6, 60
    all_point(figure=figure, angle_figure=angle_figure, color=color, start_point=start_point, angle=angle,
              length_sides=length_sides)


color_print = {'0': "COLOR_PURPLE", '1': "COLOR_RED", '2': "COLOR_ORANGE", '3': "COLOR_YELLOW", '4': "COLOR_GREEN",
               '5': "COLOR_CYAN", '6': "COLOR_BLUE"}
colors = {'0': sd.COLOR_PURPLE, '1': sd.COLOR_RED, '2': sd.COLOR_ORANGE, '3': sd.COLOR_YELLOW, '4': sd.COLOR_GREEN,
          '5': sd.COLOR_CYAN, '6': sd.COLOR_BLUE}
variable_figure = {'0': "треугольник ", '1': "квадрат", '2': "пятиугольник", '3': "шестиугольник"}
start_point = (300, 250)

print("Возможные цвета:")
for i in color_print:
    print(i, ":", color_print[i])
while True:
    input_color = input('Введите желаемый цвет >')
    if input_color in color_print:
        break
    else:
        print("Вы ввели некоректный номер ")

print("Возможные фигуры:")
for i in variable_figure:
    print(i, ":", variable_figure[i])
while True:
    input_figure = input('Введите желаемую фигуру >')
    if input_figure in variable_figure:
        if input_figure == '0':
            triangle(start_point=start_point, color=colors[input_color], angle=20, length_sides=100)
        if input_figure == "1":
            square(start_point=start_point, color=colors[input_color], angle=20, length_sides=100)
        if input_figure == "2":
            pentagon(start_point=start_point, color=colors[input_color], angle=20, length_sides=100)
        if input_figure == "3":
            hexagon(start_point=start_point, color=colors[input_color], angle=20, length_sides=100)
        break
    else:
        print("Вы ввели некоректный номер ")

print("Выполнено")

sd.pause()
