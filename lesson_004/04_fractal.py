# -*- coding: utf-8 -*-
from random import randint

import simple_draw as sd

sd.resolution = (1200, 800)


# 1) Написать функцию draw_branches, которая должна рисовать две ветви дерева из начальной точки
# Функция должна принимать параметры:
# - точка начала рисования,
# - угол рисования,
# - длина ветвей,
# Отклонение ветвей от угла рисования принять 30 градусов,

# 2) Сделать draw_branches рекурсивной
# - добавить проверку на длину ветвей, если длина меньше 5 - не рисовать
# - вызывать саму себя 2 раза из точек-концов нарисованных ветвей,
#   с параметром "угол рисования" равным углу только что нарисованной ветви,
#   и параметром "длинна ветвей" в 0.75 меньшей чем длина только что нарисованной ветви

# 3) первоначальный вызов:
# root_point = get_point(300, 30)
# draw_bunches(start_point=root_point, angle=90, length=100)

# Пригодятся функции
# sd.get_point()
# sd.get_vector()
# Возможный результат решения см lesson_004/results/exercise_04_fractal_01.jpg

# можно поиграть -шрифтами- цветами и углами отклонения

# def draw_branches(start_point , angle , length ):
#     v1 = sd.get_vector(start_point=start_point, angle=angle, length=length, width=3)
#     v1.draw()
#     if length>2:
#         next_point_paint = v1.end_point
#         angle_left=angle+30
#         angle_right = angle - 30
#         v2 = sd.get_vector(start_point=next_point_paint, angle=angle_left, length=length, width=3)
#         v2.draw()
#         v3 = sd.get_vector(start_point=next_point_paint, angle=angle_right, length=length, width=3)
#         v3.draw()
#         next_lenght= length*0.75
#         draw_branches(start_point=v2.end_point, angle=angle_left, length=next_lenght,)
#         draw_branches(start_point=v3.end_point, angle=angle_right, length=next_lenght,)
#
#
# root_point = sd.get_point(600, 30)
# draw_branches(start_point=root_point, angle=90, length=100)

# 4) Усложненное задание (делать по желанию)
# - сделать рандомное отклонение угла ветвей в пределах 40% от 30-ти градусов
# - сделать рандомное отклонение длины ветвей в пределах 20% от коэффициента 0.75
# Возможный результат решения см lesson_004/results/exercise_04_fractal_02.jpg

# Пригодятся функции
# sd.random_number()

def draw_branches(start_point, angle, length):
    v1 = sd.get_vector(start_point=start_point, angle=angle, length=length, width=3)
    v1.draw()
    if length > 2:
        next_point_paint = v1.end_point
        angle_left = angle + sd.random_number(30 * 0.6, 30 * 1.4)
        angle_right = angle - sd.random_number(30 * 0.6, 30 * 1.4)
        v2 = sd.get_vector(start_point=next_point_paint, angle=angle_left, length=length, width=3)
        v2.draw()
        v3 = sd.get_vector(start_point=next_point_paint, angle=angle_right, length=length, width=3)
        v3.draw()
        next_length = length * 0.0075 * sd.random_number(80, 120)
        draw_branches(start_point=v2.end_point, angle=angle_left, length=next_length, )
        draw_branches(start_point=v3.end_point, angle=angle_right, length=next_length, )


root_point = sd.get_point(600, 30)
draw_branches(start_point=root_point, angle=90, length=50)
sd.pause()
