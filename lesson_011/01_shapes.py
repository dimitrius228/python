# -*- coding: utf-8 -*-

import simple_draw as sd


# На основе вашего кода из решения lesson_004/01_shapes.py сделать функцию-фабрику,
# которая возвращает функции рисования треугольника, четырехугольника, пятиугольника и т.д.
#
# Функция рисования должна принимать параметры
# - точка начала рисования
# - угол наклона
# - длина стороны
#
# Функция-фабрика должна принимать параметр n - количество сторон.


def get_polygon(n):
    pass

    def drawing(start_point, angle, length_sides):
        angle_figure = 360 / n
        for _ in range(n):
            end_point = sd.vector(start=start_point, angle=angle, length=length_sides)
            angle += angle_figure
            start_point = end_point
            sd.circle(end_point, radius=2)

    return drawing


for i in range(3, 20):
    draw_triangle = get_polygon(n=i)
    draw_triangle(start_point=sd.get_point(200, 200), angle=13, length_sides=100)

sd.pause()
