# -*- coding: utf-8 -*-

# На основе своего кода из lesson_009/02_log_parser.py напишите итератор (или генератор)
# котрый читает исходный файл events.txt и выдает число событий NOK за каждую минуту
# <время> <число повторений>
#
# пример использования:
#
# grouped_events = <создание итератора/генератора>
# for group_time, event_count in grouped_events:
#     print(f'[{group_time}] {event_count}')
#
# на консоли должно появится что-то вроде
#
# [2018-05-17 01:57] 1234

# TODO здесь ваш код
from collections import defaultdict


def collect_log(file_name):
    good_count = defaultdict(int)
    for line in get_lines(file_name):
        f_data, s_time, event = line.split()
        if event == 'NOK':
            full_data = f'{f_data[1:]} {s_time[:-11]}'
            good_count[full_data] += 1
        if len(good_count) == 2:
            data = next(iter(good_count))
            count_event = good_count[data]
            good_count.pop(data)
            yield data, count_event


def get_lines(file_name):
    with open(file_name, 'r') as ff:
        for line in ff:
            if not line:
                continue
            line = line[:-1]
            yield line


grouped_events = collect_log('events.txt')
for group_time, event_count in grouped_events:
    print(f'[{group_time}] {event_count}')
