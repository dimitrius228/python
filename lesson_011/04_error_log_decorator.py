# -*- coding: utf-8 -*-

# Написать декоратор, который будет логировать (записывать в лог файл)
# ошибки из декорируемой функции и выбрасывать их дальше.
#
# Имя файла лога - function_errors.log
# Формат лога: <имя функции> <параметры вызова> <тип ошибки> <текст ошибки>
# Лог файл открывать каждый раз при ошибке в режиме 'a'


# def log_errors(func):
#     pass
#     def surrogate(*args, **kwargs):
#         try:
#             func(*args, **kwargs)
#         except Exception as exc:
#             with open('function_errors.log', mode='a', encoding='utf8') as file_log_with_error:
#                 file_log_with_error.write(f'{func.__name__} {args, kwargs} {type(exc)} {exc}\n')
#                 raise exc
#         pass
#
#     return surrogate
def file_name_log_errors(file_name_write_error):
    def give_log_func(func):
        def surrogate(*args, **kwargs):
            try:
                func(*args, **kwargs)
            except Exception as exc:
                with open(file_name_write_error, mode='a', encoding='utf8') as file_log_with_error:
                    file_log_with_error.write(f'{func.__name__} {args, kwargs} {type(exc)} {exc}\n')
                    raise exc

        return surrogate

    return give_log_func


# Проверить работу на следующих функциях
@file_name_log_errors('function_errors.log')
def perky(param):
    return param / 0


@file_name_log_errors('function_errors.log')
def check_line(line):
    name, email, age = line.split(' ')
    if not name.isalpha():
        raise ValueError("it's not a name")
    if '@' not in email or '.' not in email:
        raise ValueError("it's not a email")
    if not 10 <= int(age) <= 99:
        raise ValueError('Age not in 10..99 range')


lines = [
    'Ярослав bxh@ya.ru 600',
    'Земфира tslzp@mail.ru 52',
    'Тролль nsocnzas.mail.ru 82',
    'Джигурда wqxq@gmail.com 29',
    'Земфира 86',
    'Равшан wmsuuzsxi@mail.ru 35',
]
for line in lines:
    try:
        check_line(line)
    except Exception as exc:
        print(f'Invalid format: {exc}')
perky(param=42)

# Усложненное задание (делать по желанию).
# Написать декоратор с параметром - именем файла
