def add_bun():
    print("А теперь добавим булочку")
    return "булочка"


def add_cutlet():
    print("А теперь добавим котлету")
    return "котлета"


def add_cucumber():
    print("А теперь добавим огурчика")
    return "огурчик"


def add_tomato():
    print("А теперь добавим помидорчику")
    return "помидорчика"


def add_mayonnaise():
    print("А теперь добавим майонез")
    return "майонез"


def add_cheese():
    print("А теперь добавим сыр")
    return "сыр"


def add_ketchup():
    print("А теперь добавим кетчуп")
    return "кетчуп"


def add_onion():
    print("А теперь добавим лук")
    return "лук"
