import simple_draw as sd
import math


def my_snowflake(count_snowflake, min_point_x, max_point_x):
    y = sd.resolution[1]
    my_dict_snowflake = {}
    for i in range(count_snowflake):
        my_dict_snowflake.setdefault(i,
                                     {"length": sd.random_number(10, 100), "factor_a": sd.random_number(10, 100) / 100,
                                      "factor_b": sd.random_number(10, 100) / 100,
                                      "factor_c": sd.random_number(10, 170),
                                      "point_x": sd.random_number(min_point_x, max_point_x), "point_y": y})

    key = 0
    x, y = my_dict_snowflake[key]["point_x"], my_dict_snowflake[key]["point_y"]

    while True:
        point = sd.get_point(x, y)
        sd.start_drawing()
        sd.snowflake(center=point, length=my_dict_snowflake[key]["length"], factor_a=my_dict_snowflake[key]["factor_a"],
                     factor_b=my_dict_snowflake[key]["factor_b"], factor_c=my_dict_snowflake[key]["factor_c"])
        sd.snowflake(center=point, length=my_dict_snowflake[key]["length"], factor_a=my_dict_snowflake[key]["factor_a"],
                     factor_b=my_dict_snowflake[key]["factor_b"], factor_c=my_dict_snowflake[key]["factor_c"],
                     color=sd.background_color)
        y -= 40
        x = x + 10 * (sd.random_number(-100, 100) / 100)
        point = sd.get_point(x, y)
        sd.snowflake(center=point, length=my_dict_snowflake[key]["length"], factor_a=my_dict_snowflake[key]["factor_a"],
                     factor_b=my_dict_snowflake[key]["factor_b"], factor_c=my_dict_snowflake[key]["factor_c"])

        if y < 50:
            key = key + 1
            x, y = my_dict_snowflake[key]["point_x"], my_dict_snowflake[key]["point_y"]
            if key == count_snowflake - 1:
                key = key - count_snowflake + 1
        sd.finish_drawing()
        sd.sleep(0.1)
        if sd.user_want_exit():
            break


def my_wall(left_bottom_x, left_bottom_y, right_top_x, right_top_y):
    i = 0
    for y in range(left_bottom_y, right_top_y, 51):
        i = i + 1
        for x in range(left_bottom_x, right_top_x, 101):
            if i % 2 == 0:
                left_bottom = sd.get_point(x, y)
                if x == left_bottom_x:

                    right_top = sd.get_point(x + 50, y + 50)
                else:
                    left_bottom = sd.get_point(x - 50, y)
                    right_top = sd.get_point(x + 50, y + 50)
                sd.rectangle(left_bottom, right_top, color=sd.COLOR_DARK_ORANGE, width=0)
                continue

            left_bottom = sd.get_point(x, y)
            right_top = sd.get_point(x + 100, y + 50)
            if x + 100 > right_top_x:
                right_top = sd.get_point(x + 50, y + 50)
            sd.rectangle(left_bottom, right_top, color=sd.COLOR_DARK_ORANGE, width=0)
    return y + 50


def my_free(start_point, angle, length):
    color = sd.COLOR_DARK_YELLOW
    v1 = sd.vector(start=start_point, angle=angle, length=length, width=3, color=color)
    if length > 2:
        next_point_paint = v1
        angle_left = angle + sd.random_number(30 * 0.6, 30 * 1.4)
        angle_right = angle - sd.random_number(30 * 0.6, 30 * 1.4)
        if length < 7:
            color = sd.COLOR_GREEN
        v2 = sd.vector(start=next_point_paint, angle=angle_left, length=length, width=3, color=color)
        v3 = sd.vector(start=next_point_paint, angle=angle_right, length=length, width=3, color=color)
        next_length = length * 0.0075 * sd.random_number(80, 120)
        my_free(start_point=v2, angle=angle_left, length=next_length, )
        my_free(start_point=v3, angle=angle_right, length=next_length, )
    return 0


def smile(point_x, point_y):
    x = point_x
    y = point_y
    left_bottom = sd.get_point(x, y)
    right_top = sd.get_point(x + 90, y + 70)
    center_position_left_eye = sd.get_point(x + 30, y + 50)
    center_position_right_eye = sd.get_point(x + 60, y + 50)
    point_lines1 = sd.get_point(x + 27, y + 30)
    point_lines2 = sd.get_point(x + 37, y + 20)
    point_lines3 = sd.get_point(x + 52, y + 20)
    point_lines4 = sd.get_point(x + 62, y + 30)
    point_list_lines = [point_lines1, point_lines2, point_lines3, point_lines4]
    sd.ellipse(left_bottom, right_top, color=sd.COLOR_YELLOW, width=1)
    sd.circle(center_position_left_eye, radius=5, color=sd.COLOR_YELLOW, width=1)
    sd.circle(center_position_right_eye, radius=5, color=sd.COLOR_YELLOW, width=1)
    sd.lines(point_list_lines, color=sd.COLOR_YELLOW, closed=False, width=1)
    return x + 45, y


def rainbow(point_center):
    rainbow_colors = (sd.COLOR_RED, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN,
                      sd.COLOR_CYAN, sd.COLOR_BLUE, sd.COLOR_PURPLE)
    radius = 300
    for color in rainbow_colors:
        point_circle = point_center
        sd.circle(point_circle, radius=radius, color=color, width=15)
        radius += 15


def sun(point_center, radius):
    length_ray = radius * 1.5
    sd.circle(center_position=point_center, radius=radius, color=sd.COLOR_YELLOW, width=0)

    for angle in range(0, 360, 29):
        sd.vector(start=point_center, angle=angle, length=length_ray, width=5)


def man(length_body, point_x, point_y):
    x, y = smile(point_x=point_x, point_y=point_y, )
    point_y_body = y
    point_x_body = x
    point_body = sd.get_point(point_x_body, point_y_body)
    point_legs = sd.vector(start=point_body, angle=270, length=length_body * 0.75)
    point_hand = sd.get_point(point_x_body, point_y_body - length_body * 0.25)
    sd.vector(start=point_legs, angle=240, length=length_body * 0.75)
    sd.vector(start=point_legs, angle=300, length=length_body * 0.75)
    sd.vector(start=point_hand, angle=30, length=length_body * 0.75)
    sd.vector(start=point_hand, angle=150, length=length_body * 0.75)


def home(left_bottom_x, left_bottom_y, right_top_x, right_top_y, height_roof):
    right_top_y = my_wall(left_bottom_y=left_bottom_y, left_bottom_x=left_bottom_x, right_top_x=right_top_x,
                          right_top_y=right_top_y)
    start_point_vector = sd.get_point(left_bottom_x - 20, right_top_y)
    length_roof = right_top_x - left_bottom_x
    angle = 0
    end_point = sd.vector(start=start_point_vector, angle=angle, length=length_roof)
    length_roof_isosceles = math.sqrt((length_roof / 2) ** 2 + height_roof ** 2)
    angle_triangle = math.atan(height_roof / (length_roof / 2)) * 180 / math.pi
    angle = 180 - angle_triangle
    end_point = sd.vector(start=end_point, angle=angle, length=length_roof_isosceles)
    angle = 180 + angle_triangle
    sd.vector(start=end_point, angle=angle, length=length_roof_isosceles)
    pass


def ground(param_resolution_max_x, param_width_ground):
    left_bottom = sd.get_point(0, 0)
    right_top = sd.get_point(param_resolution_max_x, param_width_ground)
    sd.rectangle(left_bottom=left_bottom, right_top=right_top, color=sd.COLOR_DARK_GREEN, width=0)
    return param_width_ground
