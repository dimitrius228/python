# -*- coding: utf-8 -*-

# Вывести на консоль жителей комнат (модули room_1 и room_2)
# Формат: В комнате room_1 живут: ...

from room_1 import folks as folks_in_room_1
from room_2 import folks as folks_in_room_2


def print_list_folks_1(*args):
    for name in args:
        print('В комнате room_1 живут:' + name)


def print_list_folks_2(*args):
    for name in args:
        print('В комнате room_2 живут:' + name)


print_list_folks_1(*folks_in_room_1)
print_list_folks_2(*folks_in_room_2)
