# -*- coding: utf-8 -*-

# Создать пакет, в который скопировать функции отрисовки из предыдущего урока
#  - радуги
#  - стены
#  - дерева
#  - смайлика
#  - снежинок
# Функции по модулям разместить по тематике. Название пакета и модулей - по смыслу.
# Создать модуль с функцией отрисовки кирпичного дома с широким окном и крышей.

# С помощью созданного пакета нарисовать эпохальное полотно "Утро в деревне".
# На картине должны быть:
#  - кирпичный дом, в окошке - смайлик.
#  - слева от дома - сугроб (предположим что это ранняя весна)
#  - справа от дома - дерево (можно несколько)
#  - справа в небе - радуга, слева - солнце (весна же!)
# пример см. lesson_005/results/04_painting.jpg
# Приправить своей фантазией по вкусу (коты? коровы? люди? трактор? что придумается)


import simple_draw as sd
from paint_figure import paint_figure as pf

sd.resolution = (1200, 600)
print(sd.resolution[0])
with_ground = pf.ground(sd.resolution[0], param_width_ground=50)
pf.home(left_bottom_x=400, left_bottom_y=with_ground, right_top_x=800, right_top_y=with_ground + 350, height_roof=150)
pf.man(length_body=100, point_x=900, point_y=150)
pf.man(length_body=100, point_x=1040, point_y=150)
point = sd.get_point(1050, 500)
pf.sun(point_center=point, radius=70)
point = sd.get_point(100, 700)
pf.rainbow(point_center=point)
point = sd.get_point(950, 30)
pf.my_free(start_point=point, angle=90, length=40)
pf.my_snowflake(count_snowflake=5, min_point_x=50, max_point_x=300)
