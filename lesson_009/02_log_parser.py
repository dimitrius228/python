# -*- coding: utf-8 -*-

# Имеется файл events.txt вида:
#
# [2018-05-17 01:55:52.665804] NOK
# [2018-05-17 01:56:23.665804] OK
# [2018-05-17 01:56:55.665804] OK
# [2018-05-17 01:57:16.665804] NOK
# [2018-05-17 01:57:58.665804] OK
# ...
#
# Напишите программу, которая считывает файл
# и выводит число событий NOK за каждую минуту в другой файл в формате
#
# [2018-05-17 01:57] 1234
# [2018-05-17 01:58] 4321
# ...
#
# Входные параметры: файл для анализа, файл результата
# Требования к коду: он должен быть готовым к расширению функциональности. Делать сразу на классах.

import os
from pprint import pprint


class StatLog:
    def __init__(self, file):
        self.file_name = file
        self.good_stat = []
        self.good_stat_kras = {}

    def __collect(self, file):
        for line in file:
            if line.endswith('NOK\n'):
                self.good_stat.append(line)

    def collect_by_minute(self):
        os.system(r'nul>log_minute.txt')
        with open(self.file_name, 'r', encoding='utf8') as file:
            self.__collect(file)
            for line in self.good_stat:
                time = line[1:17]
                self.append_requests(time=time, path='log_minute.txt')

    def collect_by_hour(self):
        os.system(r'nul > log_hour.txt')
        with open(self.file_name, 'r', encoding='utf8') as file:
            self.__collect(file)
            for line in self.good_stat:
                time = line[1:14]
                self.append_requests(time=time, path='log_hour.txt')

    def collect_by_month(self):
        os.system(r'nul>log_month.txt')
        with open(self.file_name, 'r', encoding='utf8') as file:
            self.__collect(file)
            for line in self.good_stat:
                time = line[1:8]
                self.append_requests(time=time, path='log_month.txt')

    def collect_by_year(self):
        os.system(r'nul>log_year.txt')
        with open(self.file_name, 'r', encoding='utf8') as file:
            self.__collect(file)
            for line in self.good_stat:
                time = line[1:5]
                self.append_requests(time=time, path='log_year.txt')
            self.re_print(path)

    def re_print(self, path):
        with open(path, 'a+', encoding='utf8') as file:
            for time, count in self.good_stat_kras.items():
                file.write("\n[{time}] - {count}".format(time=time, count=count))

    def append_requests(self, time, path):
        if time in self.good_stat_kras:
            self.good_stat_kras[time] += 1
        else:
            if len(self.good_stat_kras) == 1:
                self.re_print(path)
                self.good_stat_kras = {}
            self.good_stat_kras[time] = 1


path = 'events.txt'
full_file_path = os.path.join(os.path.dirname(__file__), path)
path = os.path.normpath(full_file_path)
log = StatLog(file=path)
log.collect_by_hour()
# После выполнения первого этапа нужно сделать группировку событий
#  - по часам
#  - по месяцу
#  - по году
# Для этого пригодится шаблон проектирование "Шаблонный метод" см https://goo.gl/Vz4828
