# -*- coding: utf-8 -*-
import datetime
import os, shutil
from win32_setctime import setctime
# Нужно написать скрипт для упорядочивания фотографий (вообще любых файлов)
# Скрипт должен разложить файлы из одной папки по годам и месяцам в другую.
# Например, так:
#   исходная папка
#       icons/cat.jpg
#       icons/man.jpg
#       icons/new_year_01.jpg
#   результирующая папка
#       icons_by_year/2018/05/cat.jpg
#       icons_by_year/2018/05/man.jpg
#       icons_by_year/2017/12/new_year_01.jpg
#
# Входные параметры основной функции: папка для сканирования, целевая папка.
# Имена файлов в процессе работы скрипта не менять, год и месяц взять из времени создания файла.
# Обработчик файлов делать в обьектном стиле - на классах.
#
# Файлы для работы взять из архива icons.zip - раззиповать проводником в папку icons перед написанием кода.
# Имя целевой папки - icons_by_year (тогда она не попадет в коммит)
#
# Пригодятся функции:
#   os.walk
#   os.path.dirname
#   os.path.join
#   os.path.normpath
#   os.path.getmtime
#   time.gmtime
#   os.makedirs
#   shutil.copy2
#
# Чтение документации/гугла по функциям - приветствуется. Как и поиск альтернативных вариантов :)
# Требования к коду: он должен быть готовым к расширению функциональности. Делать сразу на классах.

# TODO здесь ваш код
import zipfile


class Zip:
    def __init__(self, zip_file_name, new_dir):
        self.zip_file_name = zip_file_name
        self.new_dir = new_dir

    def __str__(self):
        pass

    def _unzip(self):
        zfile = zipfile.ZipFile(self.file_name, 'r')
        for filename in zfile.namelist():
            zfile.extract(filename)
        self.file_name = filename

    def _check(self):
        if not zipfile.is_zipfile(self.zip_file_name):
            print("not zip-file")
            return 0

    def _check_new_dir(self, new_path):
        if not os.path.exists(new_path):
            print("нет директории {}, поэтому мы её создаём".format(new_path))
            os.makedirs(new_path)

    def __change_meto_damp(self, file_in_zipfile, full_path_on_file):
        ctime = datetime.datetime(*file_in_zipfile.date_time)
        ctime_secon = ctime.timestamp()
        tup = (ctime_secon, ctime_secon)
        setctime(full_path_on_file, ctime_secon)
        os.utime(full_path_on_file, tup)

    def unpacking_not_extract(self, zipFile, path_in_zipFile, dist_full_path_on_file):
        with zipFile.open(path_in_zipFile.filename) as in_zip_file, open(dist_full_path_on_file, 'wb') as out_zip_file:
            shutil.copyfileobj(in_zip_file, out_zip_file)

    def unpacking(self):

        if self._check() == 0:
            return 0
        with zipfile.ZipFile(self.zip_file_name, 'r') as myzip:
            for path_file_in_zipFile in myzip.infolist():
                if '.' not in path_file_in_zipFile.filename:
                    continue
                new_dir_for_writing = os.path.join(self.new_dir, str(path_file_in_zipFile.date_time[0]),
                                                   str(path_file_in_zipFile.date_time[1]))
                self._check_new_dir(new_dir_for_writing)
                full_path_on_file = os.path.join(new_dir_for_writing, os.path.basename(path_file_in_zipFile.filename))
                self.unpacking_not_extract(myzip, path_file_in_zipFile, full_path_on_file)
                self.__change_meto_damp(file_in_zipfile=path_file_in_zipFile, full_path_on_file=full_path_on_file)


path = 'icons.zip'
path = os.path.normpath(os.path.abspath(path))
dir_name = "icons_by_year"
dir_name = os.path.normpath(os.path.abspath(dir_name))
print(dir_name, path)
zip_file = Zip(path, dir_name)
zip_file.unpacking()
# Усложненное задание (делать по желанию)
# Нужно обрабатывать zip-файл, содержащий фотографии, без предварительного извлечения файлов в папку.
# Основная функция должна брать параметром имя zip-файла и имя целевой папки.
# Для этого пригодится шаблон проектирование "Шаблонный метод" см https://goo.gl/Vz4828
