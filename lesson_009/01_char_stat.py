# -*- coding: utf-8 -*-

# Подсчитать статистику по буквам в романе Война и Мир.
# Входные параметры: файл для сканирования
# Статистику считать только для букв алфавита (см функцию .isalpha() для строк)
#
# Вывести на консоль упорядоченную статистику в виде
# +---------+----------+
# |  буква  | частота  |
# +---------+----------+
# |    А    |   77777  |
# |    Б    |   55555  |
# |   ...   |   .....  |
# |    a    |   33333  |
# |    б    |   11111  |
# |   ...   |   .....  |
# +---------+----------+
# |  итого  | 9999999  |
# +---------+----------+
#
# Упорядочивание по частоте - по убыванию. Ширину таблицы подберите по своему вкусу
# Требования к коду: он должен быть готовым к расширению функциональности. Делать сразу на классах.

import operator
import zipfile
from pprint import pprint
import os


class Stat:
    def __init__(self, file_name):
        self.file_name = file_name
        self.stat = {}
        self.sorted_stat = []

    def __str__(self):
        print("-" * 35)
        print('||{name:^14} | {phone:^14}||'.format(name='Буквы', phone="частота"))
        print("-" * 35)
        print("*" * 35)
        for symbol, value in self.sorted_stat:
            print('|{name:^15} | {phone:^15d}|'.format(name=symbol, phone=value), )
            print("*" * 35)
        print("-" * 35, end="", sep='\n')
        print('\n||{name:^14} | {phone:^14d}||'.format(name='Итого', phone=self.total))
        print("-" * 35)
        return self.file_name

    def unzip(self):
        zfile = zipfile.ZipFile(self.file_name, 'r')
        for filename in zfile.namelist():
            zfile.extract(filename)
        self.file_name = filename

    def collect(self):
        self.total = 0
        if self.file_name.endswith('.zip'):
            self.unzip()
        with open(self.file_name, 'r', encoding='cp1251') as file:
            for line in file:
                self._collect_for_line(line=line)
            for count in self.stat.values():
                self.total += count
        self.descending_sort_value()

    def _collect_for_line(self, line):
        for char in line:
            if not char.isalpha():
                continue
            if char in self.stat:
                self.stat[char] += 1
            else:
                self.stat[char] = 1

    def descending_sort_value(self):
        self.sorted_stat = sorted(self.stat.items(), key=operator.itemgetter(1))
        return self.sorted_stat

    def ascending_sort_value(self):
        self.sorted_stat = sorted(self.stat.items(), key=operator.itemgetter(1))
        return self.sorted_stat

    def descending_sort_symbol(self):
        self.sorted_stat = sorted(self.stat.items(), key=operator.itemgetter(0))
        return self.sorted_stat

    def ascending_sort_symbol(self):
        self.sorted_stat = sorted(self.stat.items(), key=operator.itemgetter(0))
        return self.sorted_stat


path = 'python_snippets\\voyna-i-mir.txt'
full_file_path = os.path.join(os.path.dirname(__file__), path)
path = os.path.normpath(full_file_path)

chatterer = Stat(file_name=path)
chatterer.collect()
print(chatterer)

# После выполнения первого этапа нужно сделать упорядочивание статистики
#  - по частоте по возрастанию
#  - по алфавиту по возрастанию
#  - по алфавиту по убыванию
# Для этого пригодится шаблон проектирование "Шаблонный метод" см https://goo.gl/Vz4828
