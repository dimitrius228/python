# -*- coding: utf-8 -*-

from termcolor import cprint
from random import randint


######################################################## Часть первая
#
# Создать модель жизни небольшой семьи.
#
# Каждый день участники жизни могут делать только одно действие.
# Все вместе они должны прожить год и не умереть.
#
# Муж может:
#   есть,
#   играть в WoT,
#   ходить на работу,
# Жена может:
#   есть,
#   покупать продукты,
#   покупать шубу,
#   убираться в доме,

# Все они живут в одном доме, дом характеризуется:
#   кол-во денег в тумбочке (в начале - 100)
#   кол-во еды в холодильнике (в начале - 50)
#   кол-во грязи (в начале - 0)
#
# У людей есть имя, степень сытости (в начале - 30) и степень счастья (в начале - 100).
#
# Любое действие, кроме "есть", приводит к уменьшению степени сытости на 10 пунктов
# Кушают взрослые максимум по 30 единиц еды, степень сытости растет на 1 пункт за 1 пункт еды.
# Степень сытости не должна падать ниже 0, иначе чел умрет от голода.
#
# Деньги в тумбочку добавляет муж, после работы - 150 единиц за раз.
# Еда стоит 10 денег 10 единиц еды. Шуба стоит 350 единиц.
#
# Грязь добавляется каждый день по 5 пунктов, за одну уборку жена может убирать до 100 единиц грязи.
# Если в доме грязи больше 90 - у людей падает степень счастья каждый день на 10 пунктов,
# Степень счастья растет: у мужа от игры в WoT (на 20), у жены от покупки шубы (на 60, но шуба дорогая)
# Степень счастья не должна падать ниже 10, иначе чел умрает от депресии.
#
# Подвести итоги жизни за год: сколько было заработано денег, сколько сьедено еды, сколько куплено шуб.

class Man:

    def __init__(self, name, fullness=30, happiness=100, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name
        self.fullness = fullness
        self.happiness = happiness
        self.house = None
        self.all_money = 0
        self.all_eaten = 0
        self.all_fur_coat = 0

    def __str__(self):
        return "{} сытость - {}, счастье -  {}".format(self.name, self.fullness, self.happiness)

    def eat(self, how_many_eat):

        if self.house.eat_icebox >= how_many_eat:
            cprint('{} поел'.format(self.name), color='yellow')
            self.fullness += how_many_eat
            self.house.eat_icebox -= how_many_eat
            self.happiness += 10
            self.all_eaten += how_many_eat
        else:
            cprint('{} нет еды'.format(self.name), color='red')
        pass

    def go_to_the_house(self, house):
        self.house = house
        self.fullness -= 10
        cprint('{} Вьехал в  {}'.format(self.name, self.house), color='cyan')

    def act(self):

        if self.house.garbage > 90:
            self.happiness -= 10
        if self.happiness < 10:
            cprint("{} умер от депрессии".format(self.name), color='red')
            return False
        if self.fullness < 0:
            cprint("{} умер от голода".format(self.name), color='red')
            return False
        cprint("{} ещё жив".format(self.name), color='grey')
        if self.fullness>100:
            self.fullness=100
        if self.happiness>100:
            self.happiness=100
        if self.house.garbage>100:
            self.house.garbage=100
        return True

    def pet_the_cat(self):
        cprint('{} погладил кота'.format(self.name), color='cyan')
        self.happiness += 5

    def pick_up_cat(self, house, pet):
        pet.house = house
        cprint('{} подобрал кота {}'.format(self.name, pet.name), color='cyan')

    def bird_of_a_child(self,child):
        child.house = self.house
        self.house.child=child
        self.fullness -= 10
        cprint('В {}  появился ребёнок {}'.format(self.house,self.name ), color='cyan')


class House:

    def __init__(self, address, food_for_cat=30, money=100, eat_icebox=50, garbage=0, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.money = money
        self.eat_icebox = eat_icebox
        self.garbage = garbage
        self.address = address
        self.child=None
        self.food_for_cat = food_for_cat

    def __str__(self):
        return "{} по адрессу - {}: \nденег - {}, еды - {}, грязи - {} , еды для кота - {}".format(__class__.__name__,
        self.address,self.money, self.eat_icebox,self.garbage,self.food_for_cat)


class Husband(Man):

    def __init__(self, name):
        super().__init__(name=name)

    def __str__(self):
        return "{} {}".format(self.__class__.__name__,super().__str__())

    def act(self):
        responce = super().act()
        if responce:
            cube = randint(1, 6)
            if self.fullness < 25:
                self.eat()
            elif self.house.money < 200:
                self.work()
            elif self.happiness < 30:
                self.gaming()
            elif self.happiness > 80:
                self.work()
            elif cube == 1:
                self.work()
            elif cube == 2:
                self.gaming()
            elif cube == 3:
                self.work()
            elif cube==4:
                self.pet_the_cat()
            else:
                self.eat()
            super().act()
    def eat(self):
        how_many_eat = randint(10, 30)
        super().eat(how_many_eat=how_many_eat)

    def work(self):
        cprint("{} сходил на работу".format(self.name), color='magenta')
        self.house.money += 250
        self.all_money += 250
        self.fullness -= 10
        self.happiness -= 10
        pass

    def gaming(self):
        cprint("{} играл в игры".format(self.name), color='magenta')
        self.fullness -= 10
        if self.happiness in range(0, 80):
            self.happiness += 20
        else:
            self.happiness = 100

        pass


class Wife(Man):

    def __init__(self, name):
        super().__init__(name=name)
        pass

    def __str__(self):
        return "{} {}".format(self.__class__.__name__,super().__str__())

    def act(self):
        responce = super().act()
        if responce:
            cube = randint(1, 6)
            if self.house.eat_icebox < 35:
                self.shopping()
            elif self.fullness < 30:
                self.eat()
            elif self.house.garbage > 90:
                self.clean_house()
            elif self.happiness < 20:
                self.buy_fur_coat()
            elif cube == 1:
                if self.house.money > 400:
                    self.buy_fur_coat()
                else:
                    self.eat()
            elif cube == 2:
                self.shopping()
            elif cube == 3:
                self.clean_house()
            elif cube==4:
                self.pet_the_cat()
            else:
                self.clean_house()
            super().act()
    def eat(self):
        how_many_eat = randint(10, 25)
        super().eat(how_many_eat=how_many_eat)

    def shopping(self):
        cprint("{} сходила в магазин".format(self.name), color='magenta')
        self.house.eat_icebox += 50
        self.house.money -= 50
        self.fullness -= 10
        self.house.food_for_cat += 90
        self.house.money -=90
        self.happiness += 20


    def buy_fur_coat(self):
        cprint("{} купила шубу".format(self.name), color='magenta')
        self.house.money -= 350
        self.fullness -= 10
        self.happiness += 60
        self.all_fur_coat += 1

    def clean_house(self):
        cprint("{} убралась в доме ".format(self.name), color='magenta')
        self.house.garbage = 0
        self.fullness -= 10
        self.happiness -= 10


# home = House(address="Ленина 12")
# serge = Husband(name='Сережа')
# masha = Wife(name='Маша')
# masha.go_to_the_house(house=home)
# serge.go_to_the_house(house=home)
# for day in range(365):
#     home.garbage += 5
#     cprint('================== День {} =================='.format(day), color='red')
#     serge.act()
#     masha.act()
#     cprint(serge, color='cyan')
#     cprint(masha, color='cyan')
#     cprint(home, color='cyan')
# cprint("Всего заработанно за год - {}".format(serge.all_money), color='yellow')
# cprint("Всего съеденно за год {} - {}".format(serge.name,serge.all_eaten), color='yellow')
# cprint("Всего съеденно за год {} - {}".format(masha.name,masha.all_eaten), color='yellow')
# cprint("Всего съеденно за год  - {}".format(masha.all_eaten+serge.all_eaten), color='yellow')
# cprint("Всего купленно шуб за год - {}".format(masha.all_fur_coat), color='yellow')

######################################################## Часть вторая
#
# После подтверждения учителем первой части надо
# отщепить ветку develop и в ней начать добавлять котов в модель семьи
#
# Кот может:
#   есть,
#   спать,
#   драть обои
#
# Люди могут:
#   гладить кота (растет степень счастья на 5 пунктов)
#
# В доме добавляется:
#   еда для кота (в начале - 30)
#
# У кота есть имя и степень сытости (в начале - 30)
# Любое действие кота, кроме "есть", приводит к уменьшению степени сытости на 10 пунктов
# Еда для кота покупается за деньги: за 10 денег 10 еды.
# Кушает кот максимум по 10 единиц еды, степень сытости растет на 2 пункта за 1 пункт еды.
# Степень сытости не должна падать ниже 0, иначе кот умрет от голода.
#
# Если кот дерет обои, то грязи становится больше на 5 пунктов

class Pet:
    def __init__(self, name, fullness=30, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name
        self.fullness = fullness
        self.house = None

    def __str__(self):
        return "  имя - {}, сытость - {}".format(self.name, self.fullness)

    def act(self):
        if self.fullness < 0:
            cprint("{} умер от голода".format(self.name), color='red')
            return False
        cprint("{} ещё жив".format(self.name), color='grey')
        if self.fullness>100:
            self.fullness=100
        return True

    def eat(self, how_many_eat):
        if self.house.food_for_cat >= how_many_eat:
            cprint('{} поел'.format(self.name), color='yellow')
            self.fullness += 2 * how_many_eat
            self.house.food_for_cat -= how_many_eat
        else:
            cprint('{} нет еды'.format(self.name), color='red')
        pass


class Cat(Pet):

    def __init__(self, name):
        super().__init__(name=name)

    def __str__(self):
        return "{} {}".format(self.__class__.__name__, super().__str__())

    def act(self):
        responce = super().act()
        if responce:
            cube = randint(1, 6)
            if self.fullness < 25:
                self.eat()
            elif cube == 1:
                self.sleep()
            elif cube == 2:
                self.soil()
            elif cube == 3:
                self.eat()
            else:
                self.sleep()
            super().act()
    def eat(self):
        how_many_eat = randint(2, 10)
        super().eat(how_many_eat=how_many_eat)

    def sleep(self):
        cprint('{} спал'.format(self.name), color='yellow')
        self.fullness -= 10

    def soil(self):
        cprint('{} драл обои'.format(self.name), color='yellow')
        self.fullness -= 10
        self.house.garbage += 5


######################################################## Часть вторая бис
#
# После реализации первой части надо в ветке мастер продолжить работу над семьей - добавить ребенка
#
# Ребенок может:
#   есть,
#   спать,
#
# отличия от взрослых - кушает максимум 10 единиц еды,
# степень счастья  - не меняется, всегда ==100 ;)

class Child(Man):

    def __init__(self, name):
        super().__init__(name=name)

    def __str__(self):
        return "{} {}".format(self.__class__.__name__,super().__str__())

    def act(self):
        responce = super().act()
        if responce:
            cube = randint(1, 6)
            if self.fullness < 25:
                self.eat()
            elif cube == 1:
                self.eat()
            elif cube == 2:
                self.eat()
            elif cube == 3:
                self.eat()
            else:
                self.sleep()
            self.happiness=100
            super().act()
    def eat(self):
        how_many_eat = randint(1, 10)
        super().eat(how_many_eat=how_many_eat)


    def sleep(self):
        cprint("{} спал".format(self.name), color='magenta')
        self.fullness -= 10
        pass





######################################################## Часть третья
#
# после подтверждения учителем второй части (обоих веток)
# влить в мастер все коммиты из ветки develop и разрешить все конфликты
# отправить на проверку учителем.

#
# home = House(address="Ленина 12")
# serge = Husband(name='Сережа')
# masha = Wife(name='Маша')
# kolya = Child(name='Коля')
# # murzik = Cat(name='Мурзик')
# masha.go_to_the_house(house=home)
# serge.go_to_the_house(house=home)
# serge.bird_of_a_child(child=kolya)
# cats=[
#     Cat(name='Бивис'),
#     Cat(name='Батхед'),
#     Cat(name='Барсик'),
#     Cat(name='Яшка'),
#     Cat(name='Симка'),
#     Cat(name='Пуша'),
#     Cat(name='Смерч'),
#     Cat(name='Леопольд'),
#     Cat(name='Лаврентий'),
# ]
# for cat in cats:
#     masha.pick_up_cat(house=home,pet=cat)
#
# for day in range(365):
#     home.garbage += 5
#     cprint('================== День {} =================='.format(day), color='red')
#     serge.act()
#     masha.act()
#     kolya.act()
#     for cat in cats:
#         cat.act()
#     cprint(serge, color='cyan')
#     cprint(masha, color='cyan')
#     cprint(kolya, color='cyan')
#     for cat in cats:
#         cprint(cat, color='cyan')
#     cprint(home, color='cyan')
# cprint("Всего заработанно за год - {}".format(serge.all_money), color='yellow')
# cprint("Всего съеденно за год {} - {}".format(serge.name,serge.all_eaten), color='yellow')
# cprint("Всего съеденно за год {} - {}".format(masha.name,masha.all_eaten), color='yellow')
# cprint("Всего съеденно за год  - {}".format(masha.all_eaten+serge.all_eaten), color='yellow')
# cprint("Всего купленно шуб за год - {}".format(masha.all_fur_coat), color='yellow')
# Усложненное задание (делать по желанию)
#
# Сделать из семьи любителей котов - пусть котов будет 3, или даже 5-10.
# Коты должны выжить вместе с семьей!
#
# Определить максимальное число котов, которое может прокормить эта семья при значениях зарплаты от 50 до 400.
# Для сглаживание случайностей моделирование за год делать 3 раза, если 2 из 3х выжили - считаем что выжили.
#
# Дополнительно вносить некий хаос в жизнь семьи
# - N раз в год вдруг пропадает половина еды из холодильника (коты?)
# - K раз в год пропадает половина денег из тумбочки (муж? жена? коты?!?!)
# Промоделировать - как часто могут случаться фейлы что бы это не повлияло на жизнь героев?
#   (N от 1 до 5, K от 1 до 5 - нужно вычислит максимумы N и K при котором семья гарантированно выживает)
N=[]
K=[]
for _ in range(12):
    N.append(randint(0,365))
for _ in range(12):
    K.append(randint(0, 365))

home = House(address="Ленина 12")
serge = Husband(name='Сережа')
masha = Wife(name='Маша')
kolya = Child(name='Коля')
# murzik = Cat(name='Мурзик')
masha.go_to_the_house(house=home)
serge.go_to_the_house(house=home)
serge.bird_of_a_child(child=kolya)
cats=[
    Cat(name='Бивис'),
    Cat(name='Батхед'),
    Cat(name='Барсик'),
    Cat(name='Яшка'),
    Cat(name='Симка'),
    Cat(name='Пуша'),
    Cat(name='Смерч'),
    Cat(name='Леопольд'),
    Cat(name='Лаврентий'),
]
for cat in cats:
    masha.pick_up_cat(house=home,pet=cat)

for day in range(365):
    cprint('================== День {} =================='.format(day), color='red')
    if day in N:
        home.eat_icebox=home.eat_icebox/2
        cprint("Урали половину еды",color="red")
    if day in K:
        home.money=home.money/2
        cprint("Урали половину денег", color="red")
    home.garbage += 5
    serge.act()
    masha.act()
    kolya.act()
    for cat in cats:
        cat.act()
    cprint(serge, color='cyan')
    cprint(masha, color='cyan')
    cprint(kolya, color='cyan')
    for cat in cats:
        cprint(cat, color='cyan')
    cprint(home, color='cyan')
cprint("Всего заработанно за год - {}".format(serge.all_money), color='yellow')
cprint("Всего съеденно за год {} - {}".format(serge.name,serge.all_eaten), color='yellow')
cprint("Всего съеденно за год {} - {}".format(masha.name,masha.all_eaten), color='yellow')
cprint("Всего съеденно за год  - {}".format(masha.all_eaten+serge.all_eaten), color='yellow')
cprint("Всего купленно шуб за год - {}".format(masha.all_fur_coat), color='yellow')


# в итоге должен получится приблизительно такой код экспериментов
# for food_incidents in range(6):
#   for money_incidents in range(6):
#       life = Simulation(money_incidents, food_incidents)
#       for salary in range(50, 401, 50):
#           max_cats = life.experiment(salary)
#           print(f'При зарплате {salary} максимально можно прокормить {max_cats} котов')
