from pprint import pprint
from random import randint
from pprint import pprint

_number = {}
_bulls_and_cows = {}
_count = 0


def make_number():
    global _number

    for i in range(4):
        while True:
            input_number = randint(1, 9)
            if input_number in list(_number.values()):
                continue
            else:
                _number[i] = input_number
                break


def check_number(number):
    if len(number) != 4:
        return 0
    if not number.isnumeric():
        return 0
    set_number = set(number)
    if len(set_number) != 4:
        return 0
    global _bulls_and_cows, _count
    _bulls_and_cows = {'bulls': 0, 'cows': 0}
    _count += 1
    for i, input_number in enumerate(number):
        if int(input_number) in list(_number.values()):
            if _number[i] == int(input_number):
                _bulls_and_cows["bulls"] += 1
            else:
                _bulls_and_cows["cows"] += 1
    return _bulls_and_cows


def replay():
    global _count
    print('You win',
          "count=" + str(_count),
          'Replay?',
          "Yes - 1 ",
          "No - 2")
    while True:
        option = input("You option : ")
        if option == '1':
            _count = 0
            return 1
        elif option == '2':
            _count = 0
            return 2
        else:
            print("Некоректный ввод")
