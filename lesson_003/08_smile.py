# -*- coding: utf-8 -*-

# (определение функций)
import simple_draw
import random

# Написать функцию отрисовки смайлика в произвольной точке экрана
# Форма рожицы-смайлика на ваше усмотрение
# Параметры функции: кордината X, координата Y, цвет.
# Вывести 10 смайликов в произвольных точках экрана.
for _ in range(10):
    x = random.randint(0, 500)
    y = random.randint(0, 500)
    left_bottom = simple_draw.get_point(x, y)
    right_top = simple_draw.get_point(x + 90, y + 70)
    center_position_left_eye = simple_draw.get_point(x + 30, y + 50)
    center_position_right_eye = simple_draw.get_point(x + 60, y + 50)
    point_lines1 = simple_draw.get_point(x + 27, y + 30)
    point_lines2 = simple_draw.get_point(x + 37, y + 20)
    point_lines3 = simple_draw.get_point(x + 52, y + 20)
    point_lines4 = simple_draw.get_point(x + 62, y + 30)
    point_list_lines = [point_lines1, point_lines2, point_lines3, point_lines4]
    simple_draw.ellipse(left_bottom, right_top, color=simple_draw.COLOR_YELLOW, width=1)
    simple_draw.circle(center_position_left_eye, radius=5, color=simple_draw.COLOR_YELLOW, width=1)
    simple_draw.circle(center_position_right_eye, radius=5, color=simple_draw.COLOR_YELLOW, width=1)
    simple_draw.lines(point_list_lines, color=simple_draw.COLOR_YELLOW, closed=False, width=1)
simple_draw.pause()
