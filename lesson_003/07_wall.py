# -*- coding: utf-8 -*-

# (цикл for)
import simple_draw as sd

# Нарисовать стену из кирпичей. Размер кирпича - 100х50
# Использовать вложенные циклы for
# Использовать вложенные циклы for
brick_x, brick_y = 100, 50
left_bottom_x = 400
left_bottom_y = 50
right_top_x = 800
right_top_y = 400
height_roof = 150
row = 0
for y in range(left_bottom_y, right_top_y, brick_y + 1):
    row += 1
    for x in range(left_bottom_x, right_top_x, brick_x + 1):
        if row % 2 == 0:
            left_bottom = sd.get_point(x, y)
            if x == left_bottom_x:

                right_top = sd.get_point(x + brick_x / 2, y + brick_y)
            else:
                left_bottom = sd.get_point(x - brick_x / 2, y)
                right_top = sd.get_point(x + brick_x / 2, y + brick_y)
            sd.rectangle(left_bottom, right_top, color=sd.COLOR_DARK_ORANGE, width=0)
            continue

        left_bottom = sd.get_point(x, y)
        right_top = sd.get_point(x + 100, y + 50)
        if x + 100 > right_top_x:
            right_top = sd.get_point(x + 50, y + 50)
        sd.rectangle(left_bottom, right_top, color=sd.COLOR_DARK_ORANGE, width=0)

sd.pause()
