# -*- coding: utf-8 -*-

# День сурка
#
# Напишите функцию one_day() которая возвращает количество кармы от 1 до 7
# и может выкидывать исключения:
# - IamGodError
# - DrunkError
# - CarCrashError
# - GluttonyError
# - DepressionError
# - SuicideError
# Одно из этих исключений выбрасывается с вероятностью 1 к 13 каждый день
#
# Функцию оберните в бесконечный цикл, выход из которого возможен только при накоплении
# кармы до уровня ENLIGHTENMENT_CARMA_LEVEL. Исключения обработать и записать в лог.
# При создании собственных исключений максимально использовать функциональность
# базовых встроенных исключений.
import os

ENLIGHTENMENT_CARMA_LEVEL = 777

# TODO здесь ваш код

import random
from termcolor import cprint
class IamGodError(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message
class DrunkError(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message
class CarCrashError(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message
class GluttonyError(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message
class DepressionError(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message
class SuicideError(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message
def one_day():
    day=random.randint(1,13)
    if day==8:
        raise IamGodError('Сегодня я-БОГ')
    elif day==9:
        raise DrunkError("Я напился")
    elif day==10:
        raise CarCrashError("Я разбился на машине")
    elif day==11:
        raise GluttonyError("Я обожрался")
    elif day==12:
        raise DepressionError("Я впал в депрессию")
    elif day==13:
        raise SuicideError("Я cуициднулся")
    else:
        return day
our_carma=0
day=0
while our_carma<ENLIGHTENMENT_CARMA_LEVEL:
    try:
        day +=1
        now_day = one_day()
        our_carma += now_day
    except IamGodError as exc:
        with open('log.log', mode='a+', encoding='utf8') as ff:
            ff.write(f'сегодня {day} день\n {str(exc)}\n')
    except DrunkError as exc:
        with open('log.log', mode='a+', encoding='utf8') as ff:
            ff.write(f'сегодня {day} день\n {str(exc)}\n')
    except CarCrashError as exc:
        with open('log.log', mode='a+',encoding='utf8') as ff:
            ff.write(f'сегодня {day} день\n {str(exc)}\n')
    except GluttonyError as exc:
        with open('log.log', mode='a+', encoding='utf8') as ff:
            ff.write(f'сегодня {day} день\n {str(exc)}\n')
    except DepressionError as exc:
        with open('log.log', mode='a+', encoding='utf8') as ff:
            ff.write(f'сегодня {day} день\n {str(exc)}\n')
    except SuicideError as exc:
        with open('log.log', mode='a+', encoding='utf8') as ff:
            ff.write(f'сегодня {day} день\n {str(exc)}\n')
else:
    cprint("я свободен ",color='green')
    cprint(f'{day} день -- день освобождения',color='green')


# https://goo.gl/JnsDqu
