# -*- coding: utf-8 -*-

# Есть файл с протоколом регистраций пользователей на сайте - registrations.txt
# Каждая строка содержит: ИМЯ ЕМЕЙЛ ВОЗРАСТ, разделенные пробелами
# Например:
# Василий test@test.ru 27
#
# Надо проверить данные из файла, для каждой строки:
# - присутсвуют все три поля
# - поле имени содержит только буквы
# - поле емейл содержит @ и .
# - поле возраст является числом от 10 до 99
#
# В результате проверки нужно сформировать два файла
# - registrations_good.log для правильных данных, записывать строки как есть
# - registrations_bad.log для ошибочных, записывать строку и вид ошибки.
#
# Для валидации строки данных написать метод, который может выкидывать исключения:
# - НЕ присутсвуют все три поля: ValueError
# - поле имени содержит НЕ только буквы: NotNameError (кастомное исключение)
# - поле емейл НЕ содержит @ и .(точку): NotEmailError (кастомное исключение)
# - поле возраст НЕ является числом от 10 до 99: ValueError
# Вызов метода обернуть в try-except.

# TODO здесь ваш код
class NotNameError(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message

class NotEmailError(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message

def validate_str(line):
    try:
        name, email, how_old = line.split(' ')
    except ValueError  :
        raise ValueError('НЕ присутсвуют все три поля или присутсвуют лишние поля')
    if '@' and '.' not in email:
        raise NotEmailError('поле емейл НЕ содержит @ и .(точку)')
    if not name.isalpha():
        raise NotNameError('поле имени содержит НЕ только буквы')
    if not how_old.isnumeric():
        raise  ValueError('поле возраст НЕ является числом ')
    how_old=int(how_old)
    if not (how_old <=99 and how_old >= 10):
        raise ValueError('поле возраст число, которое не лежит от 10 до 99')
    return name,email,how_old



with open('registrations.txt',mode='r',encoding='utf8') as file_reg:
    for line in file_reg:
        line = line[:-1]
        try:
            validate_str(line)
            with open('registrations_good.log',mode='a+',encoding='utf8') as file_good:
                file_good.write(f'{line}\n')
        except ValueError as exc:
            str_for_write = f'{line} --- {exc}\n'
            with open('registrations_bad.log', mode='a+', encoding='utf8') as file_bed:
                file_bed.write(str_for_write)
        except NotEmailError as exc:
            str_for_write = f'{line} --- {exc}\n'
            with open('registrations_bad.log', mode='a+', encoding='utf8') as file_bed:
                file_bed.write(str_for_write)
        except NotNameError as exc :
            str_for_write = f'{line} --- {exc}\n'
            with open('registrations_bad.log', mode='a+', encoding='utf8') as file_bed:
                file_bed.write(str_for_write)
        except  :
            print("произошло что-то")